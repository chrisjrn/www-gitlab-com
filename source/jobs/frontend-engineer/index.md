---
layout: markdown_page
title: "Frontend Engineer"
---

## Responsibilities

* Implement the interfaces in GitLab proposed by UX Engineers and contributors
* Improve the [static website of GitLab](https://about.gitlab.com/) based on the suggestions of the Designer and CMO
* Continually improve the quality of GitLab

## Requirements for Applicants
(Check our [Jobs](https://about.gitlab.com/jobs/) page to see current openings).

* Know how to use CSS effectively
* Know how to use javascript effectively
* Collaborate effectively with UX Designers, Developers, and Designers
* Be able to work with the rest of the community
* Needs to have extensive knowledge of Rails
* Work remotely from anywhere in the world (Curious to see what that looks like?

## Priorities

You work on items tagged with 'Frontend' on [CE](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=Frontend) and [EE](https://gitlab.com/gitlab-org/gitlab-ee/issues?label_name=Frontend).

The priority starting from urgent is:

0. @mentions from team members and the rest of the community
1. Issues assigned to the upcoming milestone
2. Issues assigned to future milestones
3. Issues not assigned to a milestone

When done with a frontend issue remove the 'Frontend' label and add the next [workflow label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#workflow-labels) which is probably the 'Developer' label.
